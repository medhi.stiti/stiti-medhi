# Aucun n'import ne doit être fait dans ce fichier


def nombre_entier(n: int) -> str:
    if n == 0 :
        return "0"
    else :
        return (n*"S"+"0")



def S(n: str) -> str:
    return "S"+ n


def addition(a: str, b: str) -> str:
    if a == "0":
        return b
    elif a.startswith("S"):
        return S(addition(a[1:],b))
         


def multiplication(a: str, b: str) -> str:
    if a == "0":
        return "0"
    elif a.startswith("S"):
        return addition(b,multiplication(a[1:],b))

def facto_ite(n: int) -> int:
    if n == 0 :
        return 1
    else :
        a=1
        for i in range(2,n+1):
            a*=i
        return a
            

def facto_rec(n: int) -> int:
    if n == 0 or n == 1 :
        return 1
    else :
        return n*facto_rec(n-1)


def fibo_rec(n: int) -> int:
    if n == 0:
        return 0 
    elif n == 1:
        return 1
    else :
        return fibo_rec(n-1) + fibo_rec(n-2)
    


def fibo_ite(n: int) -> int:
    if n == 0 :
        return 0
    elif n == 1 or n == 2 :
        return 1
    else:
        courant, precedent=1, 1
        for i in range(3,n+1):
            precedent, courant = courant, precedent + courant 
        return courant
    
    
def golden_phi(n: int) -> int:
    return fibo_rec(n+1)/fibo_rec(n)
    


def sqrt5(n: int) -> int:
    return (2*golden_phi(n))-1


def pow(a: float, n: int) -> float:
    return  a**n
