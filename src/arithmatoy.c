#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "utils.h"

int VERBOSE = 0;

const char *get_all_digits() { return "0123456789abcdefghijklmnopqrstuvwxyz"; }
const size_t ALL_DIGIT_COUNT = 36;

void arithmatoy_free(char *number) { free(number); }

char *arithmatoy_add(unsigned int base, const char *lhs, const char *rhs) {
  if (VERBOSE) {
    fprintf(stderr, "add: entering function\n");
  }

  size_t lhs_length = strlen(lhs);
  size_t rhs_length = strlen(rhs);
  size_t max_length = (lhs_length > rhs_length) ? lhs_length : rhs_length;
  size_t result_length = max_length + 1;  // +1 for potential carry
  char *result = (char *)malloc((result_length + 1) * sizeof(char));
  for (int i = 0; i < result_length; i++) result[i] = '0';  // initialize to '0'
  result[result_length] = '\0';

  unsigned int carry = 0;
  size_t i = 0;

  for (; i < max_length; ++i) {
    unsigned int lhs_value = (i < lhs_length) ? get_digit_value(lhs[lhs_length - i - 1]) : 0;
    unsigned int rhs_value = (i < rhs_length) ? get_digit_value(rhs[rhs_length - i - 1]) : 0;

    unsigned int sum = lhs_value + rhs_value + carry;
    unsigned int new_carry = sum / base;
    unsigned int digit = sum % base;

    if (VERBOSE) {
      fprintf(stderr, "add: digit %u digit %u carry %u\n", lhs_value, rhs_value, carry);
      fprintf(stderr, "add: result: digit %u carry %u\n", digit, new_carry);
    }

    carry = new_carry;
    result[result_length - i - 1] = to_digit(digit);
  }

  if (carry > 0) {
    result[0] = to_digit(carry);
  }

  // Drop leading zero if there is no carry
  const char *trimmed_result = drop_leading_zeros(result);
  size_t trimmed_length = strlen(trimmed_result);

  // Copy the trimmed result into a new string
  char *new_result = (char *)malloc((trimmed_length + 1) * sizeof(char));
  strcpy(new_result, trimmed_result);

  // Free the memory allocated to the original result
  free(result);

  if (VERBOSE && carry > 0) {
    fprintf(stderr, "add: final carry %u\n", carry);
  }

  return new_result;
}





char *arithmatoy_sub(unsigned int base, const char *lhs, const char *rhs) {
  if (VERBOSE) {
    fprintf(stderr, "sub: entering function\n");
  }

  size_t lhs_length = strlen(lhs);
  size_t rhs_length = strlen(rhs);
  size_t max_length = (lhs_length > rhs_length) ? lhs_length : rhs_length;
  size_t result_length = max_length;
  char *result = (char *)malloc((result_length + 1) * sizeof(char));
  result[result_length] = '\0';

  unsigned int carry = 0;
  size_t i = 0;

  for (; i < max_length; ++i) {
    unsigned int lhs_value = (i < lhs_length) ? get_digit_value(lhs[lhs_length - i - 1]) : 0;
    unsigned int rhs_value = (i < rhs_length) ? get_digit_value(rhs[rhs_length - i - 1]) : 0;

    int difference = lhs_value - rhs_value - carry;
    if (difference < 0) {
      difference += base;
      carry = 1;
    } else {
      carry = 0;
    }

    result[result_length - i - 1] = to_digit(difference);

    if (VERBOSE) {
      fprintf(stderr, "sub: digit %d digit %d carry %d\n", lhs_value, rhs_value, carry);
      fprintf(stderr, "sub: result: digit %d carry %d\n", difference, carry);
    }
  }

  // Drop leading zero if there is no carry
  const char *trimmed_result = drop_leading_zeros(result);
  size_t trimmed_length = strlen(trimmed_result);

  // Copy the trimmed result into a new string
  char *new_result = (char *)malloc((trimmed_length + 1) * sizeof(char));
  strcpy(new_result, trimmed_result);

  // Free the memory allocated to the original result
  free(result);

  return new_result;
}





char *arithmatoy_mul(unsigned int base, const char *lhs, const char *rhs) {
  if (VERBOSE) {
    fprintf(stderr, "mul: entering function\n");
  }

  size_t lhs_length = strlen(lhs);
  size_t rhs_length = strlen(rhs);
  size_t result_length = lhs_length + rhs_length;
  char *result = (char *)malloc((result_length + 1) * sizeof(char));
  
  // Initialize result with '0's
  for (size_t i = 0; i < result_length; i++) {
    result[i] = '0';
  }
  result[result_length] = '\0';

  for (size_t i = 0; i < lhs_length; ++i) {
    unsigned int lhs_value = get_digit_value(lhs[lhs_length - i - 1]);
    unsigned int carry = 0;

    for (size_t j = 0; j < rhs_length; ++j) {
      unsigned int rhs_value = get_digit_value(rhs[rhs_length - j - 1]);

      unsigned int product = lhs_value * rhs_value + carry + get_digit_value(result[result_length - i - j - 1]);
      carry = product / base;
      unsigned int digit = product % base;

      result[result_length - i - j - 1] = to_digit(digit);

      if (VERBOSE) {
        fprintf(stderr, "mul: digit %d x %d carry %d\n", lhs_value, rhs_value, carry);
        fprintf(stderr, "mul: result: digit %d carry %d\n", digit, carry);
      }
    }

    if (carry > 0) {
      result[result_length - i - rhs_length - 1] = to_digit(carry);
    }
  }

  // Drop leading zero if present
  const char *trimmed_result = drop_leading_zeros(result);
  size_t trimmed_length = strlen(trimmed_result);

  // Copy the trimmed result into a new string
  char *new_result = (char *)malloc((trimmed_length + 1) * sizeof(char));
  strcpy(new_result, trimmed_result);

  // Free the memory allocated to the original result
  free(result);

  return new_result;
}



unsigned int get_digit_value(char digit) {
  // Convert a digit from get_all_digits() to its integer value
  if (digit >= '0' && digit <= '9') {
    return digit - '0';
  }
  if (digit >= 'a' && digit <= 'z') {
    return 10 + (digit - 'a');
  }
  return -1;
}

char to_digit(unsigned int value) {
  // Convert an integer value to a digit from get_all_digits()
  if (value >= ALL_DIGIT_COUNT) {
    debug_abort("Invalid value for to_digit()");
    return 0;
  }
  return get_all_digits()[value];
}

char *reverse(char *str) {
  // Reverse a string in place, return the pointer for convenience
  // Might be helpful if you fill your char* buffer from left to right
  const size_t length = strlen(str);
  const size_t bound = length / 2;
  for (size_t i = 0; i < bound; ++i) {
    char tmp = str[i];
    const size_t mirror = length - i - 1;
    str[i] = str[mirror];
    str[mirror] = tmp;
  }
  return str;
}

const char *drop_leading_zeros(const char *number) {
  // If the number has leading zeros, return a pointer past these zeros
  // Might be helpful to avoid computing a result with leading zeros
  if (*number == '\0') {
    return number;
  }
  while (*number == '0') {
    ++number;
  }
  if (*number == '\0') {
    --number;
  }
  return number;
}

void debug_abort(const char *debug_msg) {
  // Print a message and exit
  fprintf(stderr, debug_msg);
  exit(EXIT_FAILURE);
}

